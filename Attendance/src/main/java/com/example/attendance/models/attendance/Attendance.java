package com.example.attendance.models.attendance;

import com.example.attendance.models.subject.Subject;
import com.example.attendance.models.users.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
//import com.example.Attendance.models.users.Users;

import javax.persistence.*;
import java.util.Date;
@Data
@Builder
@Entity(name = "attendance")
@NoArgsConstructor
@AllArgsConstructor
public class Attendance {
    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    private String UUID;
    @ManyToOne
    @JoinColumn(name = "student_id")
    private User student;
    @ManyToOne
    @JoinColumn(name = "subject_id")
    private Subject subject;
    @Column
    private Date date;
    @Column
    private Boolean attendance;
}
