package com.example.attendance.models.subject;

import com.example.attendance.models.groups.Group;
import com.example.attendance.models.users.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "subject")
public class Subject {
    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    private String UUID;
    @Column
    private String name;
    @ManyToOne(cascade = CascadeType.ALL, targetEntity = Group.class)
    private Group group;

    @ManyToOne(cascade = CascadeType.ALL, targetEntity = User.class)
    private User teacher;
}
