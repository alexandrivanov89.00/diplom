package com.example.attendance.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class FirstNameAndSecondNameDto {
    String firstName;
    String secondName;
}
