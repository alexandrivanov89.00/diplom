package com.example.attendance.dto;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@Builder
public class SubjectDto {

    private String id;

    @NotBlank
    private String name;

    private String firstName;
    private String secondName;

    @NotBlank
    private String group;
    /**
     * Как передать, какой препод будет вести предмет???
     */
//    private String teacher;
}
