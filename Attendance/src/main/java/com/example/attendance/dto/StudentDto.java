package com.example.attendance.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class StudentDto {
    private String id;
    private String firstName;
    private String secondName;
}
