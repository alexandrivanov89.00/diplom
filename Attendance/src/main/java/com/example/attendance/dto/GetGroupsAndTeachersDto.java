package com.example.attendance.dto;

import com.example.attendance.models.groups.Group;
import lombok.Builder;
import lombok.Data;

import java.util.List;
@Data
@Builder
public class GetGroupsAndTeachersDto {
    List<GroupDto> groups;
    List<FirstNameAndSecondNameDto> teachers;
}
