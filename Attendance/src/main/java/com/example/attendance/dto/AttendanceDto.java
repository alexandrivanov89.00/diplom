package com.example.attendance.dto;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class AttendanceDto {
    private String id;
    private String studentId;
    private String subjectId;
    private Date date;
    private Boolean attendance;
}
//ctrl alt o - delete imports

