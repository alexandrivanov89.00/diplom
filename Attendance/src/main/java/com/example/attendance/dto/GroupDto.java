package com.example.attendance.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class GroupDto {
    private String uuid;
    private String name;
}
