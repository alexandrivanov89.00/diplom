package com.example.attendance.dto;

import com.example.attendance.models.enums.Role;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UsersDto {
    private String id;
    private Long identifyNumber;
    private String firstName;
    private String secondName;
    private String login;
    private String password;
    private Role role;
    private String group;
}
