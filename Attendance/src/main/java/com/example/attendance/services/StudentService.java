package com.example.attendance.services;

import com.example.attendance.models.attendance.Attendance;
import com.example.attendance.rep.AttendanceRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class StudentService {
    private final AttendanceRepository attendanceRepository;
    public List<Attendance> viewAtt(Date date){
        return attendanceRepository.findAllByDate(date);
    }
}
