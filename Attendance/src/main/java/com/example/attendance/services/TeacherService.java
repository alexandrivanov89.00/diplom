package com.example.attendance.services;

import com.example.attendance.dto.AttendanceDto;
import com.example.attendance.models.attendance.Attendance;
import com.example.attendance.models.groups.Group;
import com.example.attendance.rep.AttendanceRepository;
import com.example.attendance.rep.GroupsRepository;
import com.example.attendance.rep.SubjectRepository;
import com.example.attendance.rep.UsersRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@Slf4j
@RequiredArgsConstructor
public class TeacherService {
    private final GroupsRepository groupsRepository;
    private final AttendanceRepository attendanceRepository;
    private final UsersRepository usersRepository;
    private final SubjectRepository subjectRepository;
    public List<Group> getGroupsAndSubjects(){
        subjectRepository.findGroupAndSubjectByUUID("4028b881882ef40601882ef41c030000");
        return groupsRepository.findGroups();
    }


    public AttendanceDto saveAttendance(AttendanceDto attendanceDto){
        var attendance = Attendance.builder()
                .student(usersRepository.findByUUID(attendanceDto.getStudentId()))
                .attendance(attendanceDto.getAttendance())
                .subject(subjectRepository.findByUUID(attendanceDto.getSubjectId()))
                .date(attendanceDto.getDate())
                .build();
        var savedAttendance = attendanceRepository.save(attendance);
        attendanceDto.setId(savedAttendance.getUUID());
        return attendanceDto;
    }
}
