package com.example.attendance.services;

import com.example.attendance.dto.*;
import com.example.attendance.models.groups.Group;
import com.example.attendance.models.subject.Subject;
import com.example.attendance.models.users.User;
import com.example.attendance.rep.AttendanceRepository;
import com.example.attendance.rep.GroupsRepository;
import com.example.attendance.rep.SubjectRepository;
import com.example.attendance.rep.UsersRepository;
import liquibase.pro.packaged.S;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class AdminService {
    private final GroupsRepository groupsRepository;
    private final AttendanceRepository attendanceRepository;
    private final UsersRepository usersRepository;
    private final SubjectRepository subjectRepository;

    public SubjectDto saveSubject(SubjectDto subjectDto) {

        var subject = Subject.builder()
                .teacher(usersRepository.findUUIDteacher(subjectDto.getFirstName(),subjectDto.getSecondName()))
                .group(groupsRepository.findByName(subjectDto.getGroup()))
                .name(subjectDto.getName())
                .build();
        subject.setUUID(subject.getGroup().getUUID());
        var saveSubject = subjectRepository.save(subject);
        subjectDto.setId(saveSubject.getUUID());
        return subjectDto;
    }

    public GroupDto saveGroup(GroupDto groupDto) {
        var group = Group.builder()
                .name(groupDto.getName())
                .build();
        var saveGroup = groupsRepository.save(group);
        groupDto.setUuid(saveGroup.getUUID());
        return groupDto;
    }

    public UsersDto addUserStudent(UsersDto usersDto) {
        var user = User.builder()
                .group(groupsRepository.findByName(usersDto.getGroup()))//groupsRepository.findIdByName(usersDto.getGroup())
                .identifyNumber(usersDto.getIdentifyNumber())
                .roles(Collections.singleton(usersDto.getRole()))
                .firstName(usersDto.getFirstName())
                .secondName(usersDto.getSecondName())
                .login(null)
                .password(null)
                .build();
        var saveUser = usersRepository.save(user);
        usersDto.setId(saveUser.getUUID());
        return usersDto;
    }

    public UsersDto addUserTeacher(UsersDto usersDto){
        var user = User.builder()
                .group(null)//groupsRepository.findIdByName(usersDto.getGroup())
                .identifyNumber(usersDto.getIdentifyNumber())
                .roles(Collections.singleton(usersDto.getRole()))
                .firstName(usersDto.getFirstName())
                .secondName(usersDto.getSecondName())
                .login(null)
                .password(null)
                .build();
        var saveUser = usersRepository.save(user);
        usersDto.setId(saveUser.getUUID());
        return usersDto;
    }
    public GetGroupsAndTeachersDto getGroupsAndTeachers(){
        List<FirstNameAndSecondNameDto> teacher = usersRepository.findTeachers().stream()
                .map(user -> FirstNameAndSecondNameDto.builder()
                        .firstName(user.getFirstName())
                        .secondName(user.getSecondName())
                        .build()).toList();
        List<GroupDto> groups = groupsRepository.findGroups().stream()
                .map(group -> GroupDto.builder()
                        .name(group.getName())
                        .build()).toList();
        GetGroupsAndTeachersDto.builder()
                .groups(groups)
                .teachers(teacher)
                .build();


        return GetGroupsAndTeachersDto.builder()
                .groups(groups)
                .teachers(teacher)
                .build();
    }
}
//    var attendance = Attendance.builder()
//            .student(usersRepository.getById(attendanceDto.getStudentId()))
//            .attendance(attendanceDto.getAttendance())
//            .subject(subjectRepository.getById(attendanceDto.getSubjectId()))
//            .date(attendanceDto.getDate())
//            .build();
