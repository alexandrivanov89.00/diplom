package com.example.attendance.controllers;

import com.example.attendance.dto.AttendanceDto;
import com.example.attendance.models.groups.Group;
import com.example.attendance.services.TeacherService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TeacherController {

    private final TeacherService teacherService;


    public TeacherController(TeacherService teacherService) {
        this.teacherService = teacherService;
    }

    @GetMapping("/teacher")
    public List<Group> setAttendance(){

        return teacherService.getGroups();
    }

    @PostMapping("/setAtt")
    public AttendanceDto setAtt(@RequestBody AttendanceDto attendanceDto){
        return teacherService.saveAttendance(attendanceDto);
    }
}
