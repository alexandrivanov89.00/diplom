package com.example.attendance.controllers;

import com.example.attendance.dto.GroupDto;
import com.example.attendance.dto.GetGroupsAndTeachersDto;
import com.example.attendance.dto.SubjectDto;
import com.example.attendance.dto.UsersDto;
import com.example.attendance.models.enums.Role;
import com.example.attendance.services.AdminService;
import liquibase.pro.packaged.S;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class AdminController {
    private final AdminService adminService;

    public AdminController(AdminService adminService) {
        this.adminService = adminService;
    }

    @PostMapping("/admin/groups")
    public GroupDto setGroup(@RequestBody GroupDto groupDto) {
        return adminService.saveGroup(groupDto);
    }

    @PostMapping("/admin/users")
    public UsersDto setUser(@RequestBody UsersDto usersDto) {
        System.out.println(usersDto.getRole());
        if(usersDto.getRole() == Role.TEACHER){
            return adminService.addUserTeacher(usersDto);
        }else if (usersDto.getRole() == Role.STUDENT){
            return adminService.addUserStudent(usersDto);
        }
        return usersDto;
    }

    @GetMapping("/admin/subject")
    public GetGroupsAndTeachersDto getGroupsAndTeachers(){
        return adminService.getGroupsAndTeachers();
    }

    @PostMapping("/admin/subject")
    public SubjectDto addSubject(@RequestBody @Valid SubjectDto subjectDto){
        return adminService.saveSubject(subjectDto);
    }
}
