package com.example.attendance.controllers;

import com.example.attendance.models.subject.Subject;
import com.example.attendance.rep.GroupsRepository;
import com.example.attendance.rep.SubjectRepository;
import com.example.attendance.rep.UsersRepository;
import com.example.attendance.services.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class testController {
    @Autowired
    GroupsRepository groupsRepository;
    @Autowired
    SubjectRepository subjectRepository;
    @Autowired
    UsersRepository usersRepository;
    @Autowired
    AdminService adminService;
    @GetMapping("/test")
    public List<String> test(){
        return subjectRepository.findGroupAndSubjectByUUID("4028b881882ef40601882ef41c030000");
    }
}
