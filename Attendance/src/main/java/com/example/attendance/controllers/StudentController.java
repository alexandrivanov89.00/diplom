package com.example.attendance.controllers;

import com.example.attendance.models.attendance.Attendance;
import com.example.attendance.services.StudentService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@RestController
public class StudentController {
    private final StudentService studentService;

    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping("/veiwAttndance")
    public String mainWindow() {
        return "main";
    }

    @PostMapping("/viewAttendance")
    public List<Attendance> StudentAtt(Date date) {
        return studentService.viewAtt(date);
    }
}
