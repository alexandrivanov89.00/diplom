package com.example.attendance.rep;

import com.example.attendance.models.groups.Group;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface GroupsRepository extends CrudRepository<Group, String> {
    @Override
    List<Group> findAll();


    @Query(value = "select * from public.group where name = :name", nativeQuery = true)
    Group findByName(String name);

    @Query(value = "select * from public.group", nativeQuery = true)
    List<Group> findGroups();
}
