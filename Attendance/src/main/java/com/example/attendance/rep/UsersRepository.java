package com.example.attendance.rep;

//import com.example.Attendance.models.enums.Role;

import com.example.attendance.models.users.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
//import org.springframework.security.core.userdetails.UserDetails;

public interface UsersRepository extends CrudRepository<User,String> {
    User findByLogin(String login);
    User findByUUID(String uuid);
    @Query(value = "select * from public.user where uuid in (select users_id from user_role where roles = 'TEACHER')",nativeQuery = true)
    List<User> findTeachers();
    @Query(value = "select * from public.user where first_name = :firstName and second_name = :secondName",nativeQuery = true)
    User findUUIDteacher(String firstName,String secondName);
}
