package com.example.attendance.rep;

import com.example.attendance.models.attendance.Attendance;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface AttendanceRepository extends JpaRepository<Attendance,String> {
    List<Attendance> findAllByDate(Date date);
}
