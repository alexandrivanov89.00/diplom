package com.example.attendance.rep;

import com.example.attendance.models.subject.Subject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface SubjectRepository extends JpaRepository<Subject, String> {
    @Override
    List<Subject> findAll();

    Subject findByUUID(String uuid);
    @Query(value = "select name from public.subject where teacher_uuid = :uuid",nativeQuery = true)
    List<String> findGroupAndSubjectByUUID(String uuid);
}
